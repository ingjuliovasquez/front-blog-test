"use client"
import Link from 'next/link'
import { motion } from 'framer-motion'
import Navbar from '@/components/Navbar'
import { TextField } from '@mui/material'
import { useEffect, useState } from 'react'
import db from './controller'

const heroAnimation = {
    initial: { y: -100, opacity: 0 },
    animate: { y: 0, opacity: 1 },
    exit: { y: -100, opacity: 0 }
}

const textAnimation = {
    initial: { x: -100, opacity: 0 },
    animate: { x: 0, opacity: 1 },
    exit: { x: -100, opacity: 0 }
}

const initialFilters = { title: '', author: '', content: '' }

export default function ClientPage({ posts }) {

    const [items, setItems] = useState([])
    const [filters, setFilters] = useState(initialFilters)

    const aplicarFiltros = async () => {
        if (filters.title === "" && filters.author === "" && filters.content === "") {
            setItems(posts)
        } else {
            try {
                const _items = await db.getPostsFiltered(filters)
                if (_items) {
                    setItems(_items)
                }
            } catch (error) {
                alert(error)
            }
        }
    }

    const limpiarFiltros = () => {
        setFilters(initialFilters)
        setItems(posts)
    }

    useEffect(() => { setItems(posts) }, [posts])
    return (

        <main className="flex flex-col items-center relative justify-between">
            <Navbar />
            <motion.section
                {...heroAnimation}
                className='w-full hero-blog h-[300px] bg-blue-200 flex items-center justify-center'
            >
                <h1 className='text-3xl lg:text-6xl text-white font-bold' >Bienvenidos a mi blog</h1>
            </motion.section>
            <motion.div {...textAnimation} className='text-md md:text-2xl w-full sm:w-fit font-bold my-5 border py-2 px-0 sm:px-20 text-center rounded-xl' >
                <h3> Lo nuevo: </h3>
            </motion.div>
            <motion.div {...textAnimation} className='w-full flex flex-col md:flex-row gap-3 md:gap-2 lg:px-5 mx-auto items-center' >
                <span>Filtros:</span>
                <div className='w-full md:w-1/3' >
                    <TextField
                        size="small"
                        fullWidth
                        value={filters.title}
                        onChange={e => setFilters({ ...filters, title: e.target.value })}
                        label="Titulo"
                    />
                </div>
                <div className='w-full md:w-1/3' >
                    <TextField
                        size="small"
                        fullWidth
                        value={filters.author}
                        onChange={e => setFilters({ ...filters, author: e.target.value })}
                        label="Autor"
                    />
                </div>
                <div className='w-full md:w-1/3' >
                    <TextField
                        size="small"
                        fullWidth
                        value={filters.content}
                        onChange={e => setFilters({ ...filters, content: e.target.value })}
                        label="Contenido"
                    />
                </div>
                <button onClick={aplicarFiltros} className="px-5 py-2 bg-black text-white rounded-xl" >Aplicar</button>
                <button onClick={limpiarFiltros} className="px-5 py-2 bg-black text-white rounded-xl" >Limpiar</button>

            </motion.div>
            <motion.div {...heroAnimation} className='my-5 flex flex-col sm:flex-row container mx-auto gap-3 sm:gap-2' >
                {items.map((post, index) =>
                    <motion.div key={post.id}
                        {...heroAnimation}
                        transition={{ delay: 0.2 * (index + 1) }}
                        className={`w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/5 
                            h-full border rounded-xl overflow-hidden 
                            bg-white hover:bg-[#eaeaea]`
                        }
                    >
                        <Link href={`/blog/${post.slug}`} className=' h-full w-full' >
                            <div className='flex flex-col w-full'>
                                <div className="w-full relative">
                                    <img src="/blog-card.jpg" />
                                </div>
                                <div className="p-2 flex-col w-full">
                                    <h4 className='text-lg font-bold'> {post.title} </h4>
                                    <p>
                                        {
                                            post.content.length > 70
                                                ? `${post.content.substring(0, 70)}...`
                                                : post.content
                                        }
                                    </p>
                                    <h4 className='w-full text-right'> {post.author} </h4>
                                </div>
                            </div>
                        </Link>
                    </motion.div>
                )}
            </motion.div>

        </main>
    )
}
