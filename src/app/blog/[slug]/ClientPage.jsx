"use client"
import Navbar from "@/components/Navbar"
import Link from 'next/link'
import { motion } from 'framer-motion'

const textAnimation = {
    initial: { y: -100, opacity: 0 },
    animate: { y: 0, opacity: 1 },
    exit: { y: -100, opacity: 0 }
}

export default function ClientPage({ post }) {
    return <main className="w-full">
        <Navbar />
        <article className="max-w-2xl mt-20 pt-10 px-5 pb-14 mx-auto relative" >
            <motion.div {...textAnimation} transition={{ delay: 0.2 }} >
                <h1 className="text-4xl font-bold capitalize"> {post.title} </h1>
                <div className="flex items-end justify-between">
                    <span className="text-xl text-[#777777]"> {post.author} </span>
                    <span className="text-xl text-[#777777]"> {new Date(post.published).toLocaleDateString()} </span>
                </div>
            </motion.div>

            <motion.p {...textAnimation} transition={{ delay: 0.4 }} className="my-20" >
                {post.content}
            </motion.p>

            <motion.div {...textAnimation} transition={{ delay: 0.6 }}>
                <Link href="/" className="hover:text-[#aaaaaa]" > ←  Regresar al inicio</Link>
            </motion.div>

        </article>
    </main>
}
