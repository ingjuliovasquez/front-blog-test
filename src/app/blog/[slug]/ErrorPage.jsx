"use client"
import Navbar from "@/components/Navbar"
import Link from 'next/link'

export default function ErrorPage() {
    return <main>
        <Navbar />
        <section className="mt-20 p-10 max-w-4xl mx-auto" >
            <h2 className="text-6xl text-center mb-20" > No se encontró la página</h2>
            <Link href="/" className="mt-20 hover:text-[#aaaaaa]" > ←  Regresar al inicio</Link>
        </section>
    </main>
}
