import db from '../../controller'
import ClientPage from './ClientPage'
import ErrorPage from './ErrorPage'
export default async function page({ params }) {
  const { slug } = params;
  const post = await db.getPost(slug)
  if (!post) { return <ErrorPage /> }
  return <ClientPage post={post} />
}
