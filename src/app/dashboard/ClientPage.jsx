"use client"
import Navbar from "@/components/Navbar"
import { DataGrid } from "@mui/x-data-grid"
import { useEffect, useState } from 'react'
import db from '../controller'
import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    TextField,
} from '@mui/material';

const initialForm = {
    open: false,
    id: null,
    author: '',
    content: '',
    title: '',
    slug: ''
}

export default function ClientPage() {

    const [posts, setPosts] = useState([])
    const [form, setForm] = useState(initialForm)
    const [seleccionado, setSeleccionado] = useState([])
    const [dialogConfirmaBorrar, setDialogConfirmaBorrar] = useState(false)

    async function getPosts() {
        try {
            const _posts = await db.getPosts()
            if (_posts) { setPosts(_posts) }
        } catch (error) { alert(error) }
    }

    useEffect(() => { getPosts() }, [])



    const columns = [
        { field: 'id', headerName: 'ID', width: 50 },
        { field: 'slug', headerName: 'Slug', width: 300 },
        { field: 'title', headerName: 'Título', width: 250 },
        { field: 'author', headerName: 'Autor', width: 150 },
        { field: 'content', headerName: 'Contenido', width: 350 },
    ];


    const abrirForm = () => setForm({ ...form, open: true })
    const cerrarForm = () => setForm({ ...form, open: false })
    const reiniciarForm = () => { setForm(initialForm) }

    const abrirDialogConfirmaBorrar = () => setDialogConfirmaBorrar(true)
    const cerrarDialogConfirmaBorrar = () => setDialogConfirmaBorrar(false)

    const openEdit = () => {
        const _post = posts.find(post => post.id === seleccionado[0])
        setForm({ ..._post, open: true })
    }

    const confirmaBorrar = async () => {
        const _post = posts.find(post => post.id === seleccionado[0])
        try {
            await db.deletePost(_post.slug)
            await db.getPosts()
            setSeleccionado([])
            cerrarDialogConfirmaBorrar()
        } catch (error) {
            alert(error)
        }
    }

    async function guardar() {
        try {
            const payload = { ...form }
            delete payload.open;
            if (!form.id) {
                console.log("Se está guardando")
                delete payload.id
                const result = await db.createPost(payload)
                alert(result)
            } else {
                delete payload.id
                const result = await db.updatePost(payload.slug, payload)
                alert(result)
            }
            await getPosts()
            reiniciarForm()
        } catch (error) {
            alert(error)
        }
    }

    const submitForm = (e) => {
        e.preventDefault()
        guardar()
    }


    return <main className="w-full">
        <Navbar />
        <section className="mt-20 pt-10 max-w-2xl mx-auto" >
            <div className="flex items-center justify-between" >
                <h2 className="text-3xl font-bold" >Posts</h2>
                <div className="flex items-center gap-2">
                    {
                        seleccionado.length === 1
                            ? <>
                                <button onClick={openEdit} className="px-5 py-2 bg-black text-white rounded-xl" >Editar</button>
                                <button onClick={abrirDialogConfirmaBorrar} className="px-5 py-2 bg-red-400 text-white rounded-xl" >Eliminar</button>
                            </>
                            : <button onClick={abrirForm}
                                className="px-5 py-2 bg-black text-white rounded-xl" >Agregar</button>
                    }
                </div>
            </div>

            <div className="h-[300px] w-full mt-10">
                <DataGrid rows={posts} columns={columns}
                    onRowSelectionModelChange={(sel) => {
                        if (seleccionado[0] != sel[0]) {
                            setSeleccionado(sel);
                        } else {
                            setSeleccionado([])
                        }
                    }}
                    rowSelectionModel={seleccionado} />
            </div>

            <Dialog open={form.open} onClose={cerrarForm} fullWidth maxWidth="sm">
                <form onSubmit={submitForm}>
                    <DialogTitle>
                        Post
                    </DialogTitle>
                    <DialogContent>

                        <div className="flex flex-col w-full gap-3 mt-2" >
                            <TextField
                                value={form.title}
                                onChange={e => setForm({ ...form, title: e.target.value })}
                                label="Título"
                                size="small"
                                required
                            />
                            <TextField
                                value={form.slug}
                                onChange={e => setForm({ ...form, slug: e.target.value })}
                                label="Slug (Debe ser único)"
                                size="small"
                                required
                                disabled={form.id != null}
                            />
                            <TextField
                                value={form.author}
                                onChange={e => setForm({ ...form, author: e.target.value })}
                                label="Autor"
                                size="small"
                                required
                            />
                            <TextField
                                value={form.content}
                                onChange={e => setForm({ ...form, content: e.target.value })}
                                label="Contenido"
                                size="small"
                                multiline
                                rows={6}
                                required
                            />
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <div className="flex gap-3">
                            <button type="submit" className="px-5 py-2 bg-black text-white rounded-xl" >Guardar</button>
                            <button
                                onClick={reiniciarForm}
                                className="px-5 py-2 bg-red-400 text-white rounded-xl"
                            >
                                Cancelar
                            </button>
                        </div>
                    </DialogActions>
                </form>
            </Dialog>

            <Dialog open={dialogConfirmaBorrar} onClose={cerrarDialogConfirmaBorrar} >
                <DialogTitle>
                    Atención
                </DialogTitle>
                <DialogContent>
                    ¿Seguro que desea borrar el elemento seleccionado?
                </DialogContent>
                <DialogActions>
                    <button onClick={confirmaBorrar} className="px-5 py-2 bg-red-400 text-white rounded-xl" >Eliminar</button>
                    <button
                        onClick={cerrarDialogConfirmaBorrar}
                        className="px-5 py-2 bg-black text-white rounded-xl"
                    >
                        Cancelar
                    </button>
                </DialogActions>
            </Dialog>


        </section>
    </main>
}
