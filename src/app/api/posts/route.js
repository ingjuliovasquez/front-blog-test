import { NextResponse } from 'next/server'
import { prisma } from '../../../../prisma/client'

export async function POST(req) {
    const data = await req.json()
    let res = "Post guardado"
    try {
        await prisma.posts.create({ data })
    } catch (error) {
        res = error.code === "P2002"
            ? "Ya existe un post con ese slug"
            : "No se ha podido guardar debido a un error desconocido. Código: " + error.code
    }
    return NextResponse.json(res)
}

export async function GET(req) {
    const { searchParams } = new URL(req.url)
    const title = searchParams.get("title")
    const author = searchParams.get("author")
    const content = searchParams.get("content")
    let filters = []
    if (title) { filters.push({ title: { contains: title } }) }
    if (author) { filters.push({ author: { contains: author } }) }
    if (content) { filters.push({ content: { contains: content } }) }
    let posts = []
    if (!title && !author && !content) {
        posts = await prisma.posts.findMany()
    } else if (title || author || content) {
        posts = await prisma.posts.findMany({ where: { AND: filters } })
    }
    return NextResponse.json(posts)
}