import { NextResponse } from 'next/server'
import { prisma } from '../../../../../prisma/client'

export async function GET(_, { params }) {
    const { slug } = params
    const post = await prisma.posts.findFirst({ where: { slug } })
    return NextResponse.json(post)
}

export async function PUT(req, { params }) {
    const data = await req.json()
    const { slug } = params
    await prisma.posts.update({ where: { slug }, data })
    return NextResponse.json("Post actualizado")
}

export async function DELETE(_, { params }) {
    const { slug } = params
    await prisma.posts.delete({ where: { slug } })
    return NextResponse.json("Post eliminado")
}