import axios from "axios";
const api = (process.env.URL || "http://localhost:3000")
    + "/api/posts/"

function getPosts() {
    return new Promise(async (resolve, reject) => {
        try {
            const { data } = await axios.get(api)
            resolve(data)
        } catch (error) {
            reject(error)
        }
    })
}

function getPost(slug) {
    return new Promise(async (resolve, reject) => {
        try {
            const { data } = await axios.get(`${api}${slug}`)
            resolve(data)
        } catch (error) {
            reject(error)
        }
    })
}

function getPostsFiltered(filters) {
    return new Promise(async (resolve, reject) => {
        let _filters = []
        if (filters.title != "") { _filters.push(`title=${filters.title}`) }
        if (filters.author != "") { _filters.push(`author=${filters.author}`) }
        if (filters.content != "") { _filters.push(`content=${filters.content}`) }

        const params = _filters.join("&")
        const url = `${api}?${params}`
        try {
            const { data } = await axios.get(url)
            resolve(data)
        } catch (error) {
            reject(error)
        }
    })
}

function createPost(payload) {
    return new Promise(async (resolve, reject) => {
        try {
            const { data } = await axios.post(api, payload)
            resolve(data)
        } catch (error) {
            reject(error)
        }
    })
}

function updatePost(slug, payload) {
    return new Promise(async (resolve, reject) => {
        try {
            const { data } = await axios.put(`${api}${slug}`, payload)
            resolve(data)
        } catch (error) {
            reject(error)
        }
    })
}

function deletePost(slug) {
    return new Promise(async (resolve, reject) => {
        try {
            const { data } = await axios.delete(`${api}${slug}`)
            resolve(data)
        } catch (error) {
            reject(error)
        }
    })
}

export default { getPosts, getPost, getPostsFiltered, createPost, updatePost, deletePost }