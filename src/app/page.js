import db from './controller'
import ClientPage from './ClientPage'
export default async function Home() {
  const posts = await db.getPosts()
  return <ClientPage posts={posts} />
}
