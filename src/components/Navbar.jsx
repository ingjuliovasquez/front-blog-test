"use client"
import React from 'react'
import Link from 'next/link'

export default function Navbar() {
  return (
    <nav className='bg-black text-white fixed top-0 w-full z-10 flex items-center justify-between px-5 py-1'>
        <Link href="/" className='px-10 py-2 hover:bg-white hover:text-black rounded-xl'>Blog</Link>
        <Link href="/dashboard" className='px-10 py-2 hover:bg-white hover:text-black rounded-xl' >Dashboard</Link>
    </nav>
  )
}
